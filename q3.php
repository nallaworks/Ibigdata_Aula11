
<?php
include './autoload.php'; //script para carregar a biblioteca de Machine Learning


$tokenizer = new HybridLogic\Classifier\Basic;
$classifier = new HybridLogic\Classifier($tokenizer);


$classifier->train('Certo', '');
$classifier->train('Certo', '');
$classifier->train('Certo', '');



$classifier->train('Meio', '');
$classifier->train('Meio', '');
$classifier->train('Meio', '');

$classifier->train('Errado', '');
$classifier->train('Errado', '');
$classifier->train('Errado', '');

echo "1984 é um livro que relata o monitoramento constante da sociedade por um governo. Comente como na Aula 1, conseguimos visualizar os dados em tempo Real do Twitter e quais seriam as possíveis aplicações desses dados? \n";
// abrir arquivo csv em modo de leitura
$res = fopen('respostas.csv', "r");
// obter os dados em cada linha
while (($data = fgetcsv($res, 100000,";")) !== FALSE) {

    $texto = "\"".$data[3]."\"";
    $groups = $classifier->classify($texto);
    echo $data[0]."| Certo = ".number_format($groups['Certo']*100,2)." | Meio = ".number_format($groups['Meio']*100,2)." | Errado = ".number_format($groups['Errado']*100,2)."\n";
    //var_dump($groups);
}
// fechar o fecha csv
fclose($res);
