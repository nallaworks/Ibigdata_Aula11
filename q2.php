
<?php
include './autoload.php'; //script para carregar a biblioteca de Machine Learning


$tokenizer = new HybridLogic\Classifier\Basic;
$classifier = new HybridLogic\Classifier($tokenizer);


$classifier->train('Certo', 'Quando se trabalha com dados é preciso ter um padrão para conseguir trabalhar como por exemplo em dados do tipo data onde antes de utiliza-lo é preciso saber a forma com que é feita a separação dos dados "/" "-", no caso de valores também é possível tratar espaço, virgulas, limites, e também é preciso analisar se toda a informação é relevante como por exemplo o caso de espaços vazios e números incorretos.');
$classifier->train('Certo', 'É aplicada as tarefas de mineração de dados, classificação, associação onde é analisado os resultados obtidos por cada método de cada tarefa.');
$classifier->train('Certo', 'Esse tratamento é feito através de uma mineração de dados que organiza os dados de uma maneira que se cria regras e padrões. Esse tratamento é feito para saber quais dados realmentes são importantes. Ao chegar no final de uma análise de dados iremos ter todos nossos dados que antes estavam desorganizados, transformados de um melhor modo e mais bonito para se ler e interpretar.');

$classifier->train('Meio', 'Devemos sempre ter certeza que os dados apresentados são verdadeiros, com isso aumentamos a porcentagem de certeza. Porque ao manipular uma grande variedade de dados, temos a chance de encontrar falacias, brincadeiras, erros, entre outros que iriam decair nossa analise. Devemos filtrar-los para retirar os erros e tentar deixar o mais limpo possível.');
$classifier->train('Meio', 'Como é uma ferramenta de mineração de dados, muito usada por estudantes e professores temos que tratar essas informações quanto a veracidade, pois os dados podem ser ou não ser confiáveis.');
$classifier->train('Meio', 'o tratamento de dados serve como um auxílio prévio para o desempenho e precisão da análise que será feita posteriormente. um exemplo que pode ser citado é o tratamento de uma imagem digital antes da aplicação de um algoritmo de detecção de cores. Sem esse tratamento prévio. o desempenho e a precisão do processo de execução do algoritmo será comprometido, chegando ao ponto de ser descartado por não atender os requisito preestabelecidos. ');

$classifier->train('Errado', 'primeiro a gente pega os dados depois enfia eles goela abaixo da primeira criatura que aparecer na sua frente, fim');
$classifier->train('Errado', 'Através de mineração de dados.');
$classifier->train('Errado', 'classificação dos tipos de dados.');

echo "Weka é uma boa ferramenta para análise de dados, porém temos que realizar um tratamento dos dados antes de iniciar uma análise. Comente como é feito esse tratamento em relação a tipos de dados? \n";
// abrir arquivo csv em modo de leitura
$res = fopen('respostas.csv', "r");
// obter os dados em cada linha
while (($data = fgetcsv($res, 100000,";")) !== FALSE) {

    $texto = "\"".$data[2]."\"";
    $groups = $classifier->classify($texto);
    echo $data[0]."| Certo = ".number_format($groups['Certo']*100,2)." | Meio = ".number_format($groups['Meio']*100,2)." | Errado = ".number_format($groups['Errado']*100,2)."\n";
    //var_dump($groups);
}
// fechar o fecha csv
fclose($res);
